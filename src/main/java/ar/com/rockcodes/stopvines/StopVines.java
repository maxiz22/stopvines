package ar.com.rockcodes.stopvines;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class StopVines extends JavaPlugin implements Listener {

	public static StopVines plugin;
	
    @Override
    public void onEnable() {
    		StopVines.plugin = this;
            Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }
   
    @EventHandler
    public void onChat(BlockSpreadEvent e) {
        Block block = e.getBlock();
        
        if (block.getType().equals( Material.VINE)){
            e.setCancelled(true);
        }
    }
}
